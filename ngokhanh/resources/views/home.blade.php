@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <nav>
                        <a href="{{route('admin.getlist')}}">{{ trans('message.Home') }} |</a>
                        <a href="{{route('admin.getadd')}}">| {{ trans('message.Add') }}</a>
                        <a href="/language/en">| English</a>
                        <a href="/language/vi">| Vietnam</a>
                    </nav>
                </div>

                <div class="card-body" >
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @yield('noidung')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
