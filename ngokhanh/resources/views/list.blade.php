@extends('home')
@section('noidung')
<div style="overflow: auto;">
	<table class="table" id="tableuser">
	    <thead>
	      <tr>
	        <th>Tên</th>
	        <th>Email</th>
	        <th>Password</th>
	        <th>Xóa</th>
	        <th>Sửa</th>
	      </tr>
	    </thead>
	    <tbody>
	    @foreach($users as $user)
	      <tr class="id{{$user->id}}">
	        <td>{{$user->name}}</td>
	        <td>{{$user->email}}</td>
	        <td>{{$user->password}}</td>
	        <td><a href="#" class="delete" id="{{$user->id}}">xoa</a></td>
	        <td><a href="/admin/edit/{{$user->id}}">Sửa</a></td>
	      </tr>
	     @endforeach
	    </tbody>
	</table>
</div>
{{$users->links()}}
<script type="text/javascript">
	$(document).on('click','.delete',function(){
		var id= $(this).attr('id');
		$.ajax({
			url: "{{route('admin.getdelete')}}",
			mehtod:"get",
			data: {id:id},
			success:function(data)
			{
				$('tbody tr.id'+id).remove();
			}
		})
	});
</script>
@endsection()