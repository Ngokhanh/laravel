@extends('home')
@section('noidung')
@foreach($edit as $sua)
<form method="POST" action="{{route('admin.postedit')}}">
	 <input type="hidden" name="_token" value="{!! csrf_token() !!}">
   <input type="hidden" name="txtid" value="{{$sua->id}}">
    <div class="form-group">
      <label for="usr">Name:</label>
      <input type="text" class="form-control" id="usr" name="txtname" value="{{$sua->name}}">
    </div>
    <div class="form-group">
      <label for="usr">Email:</label>
      <input type="text" class="form-control" id="usr" name="txtemail" value="{{$sua->email}}">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" name="txtpass" value="{{$sua->password}}">
    </div>
     <button type="submit" class="btn btn-default">Save</button>
 </form>
 @endforeach
@endsection()