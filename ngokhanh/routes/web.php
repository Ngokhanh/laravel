<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/language/{locale}', 'HomeController@index')->name('home');
Route::group(['prefix'=>'admin','middleware'=>'guest'], function () {
	Route::get('/list',['as'=>'admin.getlist','uses'=>'usercontroller@getlist']);
	Route::get('/delete',['as'=>'admin.getdelete','uses'=>'usercontroller@getdelete']);
	Route::get('/add',['as'=>'admin.getadd','uses'=>'usercontroller@getadd']);
	Route::post('/add',['as'=>'admin.postadd','uses'=>'usercontroller@postadd']);
	Route::get('/edit/{id}',['as'=>'admin.getedit','uses'=>'usercontroller@getedit']);
	Route::post('/update',['as'=>'admin.postedit','uses'=>'usercontroller@postedit']);
	Route::get('/logout',['as'=>'admin.getlogout','uses'=>'usercontroller@getlogout']);
});
Route::get('/login',['as'=>'admin.getlogin','uses'=>'usercontroller@getlogin']);
Route::post('/login',['as'=>'admin.postlogin','uses'=>'usercontroller@postlogin']);