<!DOCTYPE html>
<html>
<head>
    <title>Đăng Nhập</title>
    <!-- Bootrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet"> <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="{{ url('admin/assets/scss/login.css') }}">
    <!-- End Bootrap -->

</head>
<body>
@if (session('logout'))
    <div class="alert alert-success">{{session('logout')}}</div>
@endif
@if (session('status'))
    <div class="alert alert-danger">{{session('status')}}</div>
@endif
@if (count($errors) > 0)
     <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
          {{ $error }}</br>
        @endforeach
     </div>
@endif 
<!-- noi dung -->
<div class="container-fluid"> 
 <div class="row-fluid">
  <div class="col-md-offset-4 col-md-4" id="box">
   <h2>Admin</h2> 
   <hr> 
   <form class="form-horizontal" action="{{route('admin.getlogin')}}" method="POST" id="login_form">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <fieldset> 
     <div class="form-group"> 
      <div class="col-md-12"> 
       <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> <input name="txt_name" placeholder="Email" class="form-control" type="email"> 
       </div> 
      </div> 
     </div> 
     <div class="form-group"> 
      <div class="col-md-12"> 
       <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span> <input name="txt_pass" placeholder="Password" class="form-control" type="Password"> 
       </div> 
      </div> 
     </div> 
     <div class="form-group"> 
      <div class="col-md-12">
       <button type="submit" class="btn btn-md btn-danger pull-right">Đăng nhập </button> 
      </div> 
     </div> 
    </fieldset> 
   </form> 
  </div> 
 </div>
</div>
<!--End noi dung -->

<script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>
<script type="text/javascript">
    $("div.alert").delay(3000).slideUp();

      function confirmAction() {
        return confirm("Bạn muốn xóa không?")
      }
</script>

</body>
</html>