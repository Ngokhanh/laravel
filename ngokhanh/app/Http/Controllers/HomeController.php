<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session,App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {   
        Session::put('locale', $locale);
        return redirect()->back();
    }
}
