<?php
/**
 * Created by PhpStorm.
 * User: Ngo Khanh
 * Date: 18/05/2018
 * Time: 2:51 CH
 */

namespace App\Repositories;


interface userRepositories
{
    public function  getAll();

    public function getByid($id);

    public function create(array $attributes);

    public function update($id,array $attributes);

    public function delete($id);

    public function getupdate($id);
}