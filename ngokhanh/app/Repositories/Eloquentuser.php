<?php
/**
 * Created by PhpStorm.
 * User: Ngo Khanh
 * Date: 18/05/2018
 * Time: 2:54 CH
 */

namespace App\Repositories;


use App\User;

class Eloquentuser implements userRepositories
{
    /**
     * @var User
     */
    /**
     * Eloquentuser constructor.
     *
     */
    private $user;
    public function __construct(User $user)
    {
        $this->user = $user;

    }

    public function getAll()
    {

        return $this->user->all();

    }

    public function getByid($id)
    {
        return $this->user->find($id);
    }

    public function create(array $attributes)
    {
       return $this->user->insert(['name'=>$attributes[0],'email'=>$attributes[1],'password'=>$attributes[2]]);
    }

    public function update($id, array $attributes)
    {
        return $this->user->where('id', $id)->update(['name'=>$attributes[0],'email'=>$attributes[1],'password'=>$attributes[2]]);
    }

    public function delete($id)
    {
        $this->getByid($id)->delete();
        return true;
    }

    public function getupdate($id){

        return $this->user->select('id','name','email','password')->where('id',$id);

    }

}