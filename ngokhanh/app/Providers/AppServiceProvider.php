<?php

namespace App\Providers;

use App\Repositories\Eloquentuser;
use App\Repositories\userRepositories;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(userRepositories::class,Eloquentuser::class);
    }
}
