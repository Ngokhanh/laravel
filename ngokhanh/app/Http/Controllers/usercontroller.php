<?php

namespace App\Http\Controllers;

use App\Repositories\userRepositories;
use Illuminate\Http\Request;
use App\User;
use Hash,App,Session;
use Illuminate\Support\Facades\Auth;
class usercontroller extends Controller
{
    protected $user;
    public function __construct(userRepositories $user)
    {
        $this->user=$user;
    }
    public function getlist(){
        
        App::setLocale(Session::get('locale'));
        $users = $this->user->getAll();
        return view('list', ['users' => $users]);
    }
    public function getdelete(Request $request){
        $this->user->delete($request->id);     
    }
    public function getadd(){

        App::setLocale(Session::get('locale'));
        return view('add');

    }
    public function postadd(Request $request){
        $name = $request->txtname;
        $email = $request->txtemail;
        $pass = Hash::make($request->txtpass);
        $data =[$name,$email,$pass];
        $this->user->create($data);
        return redirect()->route('admin.getlist');
    }
    public function getedit($id){
        App::setLocale(Session::get('locale'));
        $edit= $this->user->getupdate($id)->get();
        return view('edit',['edit'=>$edit]);
    }

    public function postedit(Request $request){
        $id=$request->txtid;
        $name = $request->txtname;
        $email = $request->txtemail;
        $pass = Hash::make($request->txtpass);
        $data =[$name,$email,$pass];
        $this->user->update($id,$data);
        return redirect()->route('admin.getlist');
    }
    public function getlogin(){

        return view('login');

    }
    public function postlogin(Request $request){
        $email=$request->txt_name;
        $pass=$request->txt_pass;
        $name='ngo khanh';
        if (Auth::attempt(['email' => $email, 'password' => $pass,'name' => $name])) {
            
            return redirect()->route('admin.getlist')->with(['status'=>'Đăng nhập thành công']);

        }else{
            return redirect()->route('admin.getlogin')->with(['status'=>'Đăng nhập không thành công']);
        }

    }
    public function getlogout(){

        Auth::logout();
        return redirect()->route('admin.getlogin')->with(['status'=>'Đăng xuất thành công']);
    }

}
